import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';

import ru from '../i18n/lang-ru';
import en from '../i18n/lang-en';

Vue.use(Vuetify);

export default new Vuetify({
  lang: {
    locales: { ru, en },
  },
});
