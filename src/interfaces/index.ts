/* eslint-disable camelcase */
interface WeatherResponse {
  weather: [
    {
      id: number,
      main: string,
      description: string,
      icon: string,
    },
  ],
  main: {
    temp: number,
    feels_like: number,
    temp_min: number,
    temp_max: number,
    pressure: number,
    humidity: number,
  },
  wind: {
    speed: number,
    deg: number,
  },
  id: number,
  cod: number,
}

interface LangDataInterface {
  value: string,
  label: string,
  system: string,
  units: string,
}

export {
  WeatherResponse,
  LangDataInterface,
};
