export const ENDPOINT = 'https://api.openweathermap.org/data/2.5/weather';
export const ICON_ENDPOINT = 'http://openweathermap.org/img/wn';
export const LANG = {
  RU: 'ru',
  EN: 'en',
};
